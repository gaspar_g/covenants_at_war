#!/usr/bin/env python3

from cards_testing_simple_ui import perform_card_testing


def main():
    perform_card_testing()


if __name__ == '__main__':
    main()
