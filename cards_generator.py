#!/usr/bin/env python3

from enum import Enum, auto, unique
from itertools import combinations_with_replacement
from typing import List, Optional, NamedTuple


ALL_POSSIBLE_STRENGTH_VALUES = [1, 2, 3, 4]
REQUIRED_SUM_OF_STRENGTH_AND_SYMBOLS_FOR_EVERY_CARD = 4
MULTIPLIER_OF_CARDS_WITHOUT_COLOR = 24


@unique
class Color(Enum):

    brown = auto()      # brązowy / wojownik
    gray = auto()       # szary / łotrzyk
    purple = auto()     # fioletowy / kleryk
    orange = auto()     # pomarańczowy / czarodziej
    red = auto()        # Fire Mana / Mana Ognia
    blue = auto()       # Water Mana / Mana Wody
    green = auto()      # Earth Mana / Mana Ziemi
    yellow = auto()     # Air Mana / Mana Powietrza


@unique
class Symbol(Enum):

    # Wojownik:
    sword = auto()       # Miecz
    arrow = auto()       # Strzała
    shield = auto()      # Tarcza
    helmet = auto()      # Hełm

    # Łotrzyk:
    coin = auto()        # Moneta
    eye = auto()         # Oko
    mask = auto()        # Maska
    shuriken = auto()    # Shuriken

    # Kleryk:
    chalice = auto()     # Kielich
    sun = auto()         # Słońce
    moon = auto()        # Księżyc
    leaf = auto()        # Liść

    # Czarodziej:
    alembic = auto()     # Alembik
    skull = auto()       # Czaszka
    book = auto()        # Księga
    wand = auto()        # Różdżka

    def __str__(self):
        return self.name


class TestCard(NamedTuple):

    symbols: List[Symbol]
    strength: int
    color: Optional[Color]

    def __repr__(self):
        symbols_repr = f'[{" ".join(sym.name for sym in self.symbols)}]'
        color_repr = (self.color.name if self.color is not None
                      else '---')
        return f'<Symbols={symbols_repr} strength={self.strength} color={color_repr}>'


def generate_all_test_cards():
    for strength in ALL_POSSIBLE_STRENGTH_VALUES:
        number_of_symbols = REQUIRED_SUM_OF_STRENGTH_AND_SYMBOLS_FOR_EVERY_CARD - strength
        for symbols in generate_combinations_of_symbols(number_of_symbols):
            for color in generate_colors_and_empty():
                yield TestCard(symbols, strength, color)


def generate_combinations_of_symbols(number_of_symbols):
    for combination in combinations_with_replacement(Symbol, number_of_symbols):
        yield combination


def generate_colors_and_empty():
    for i in range(MULTIPLIER_OF_CARDS_WITHOUT_COLOR):
        yield None
    yield from Color
