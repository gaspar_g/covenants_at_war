#!/usr/bin/env python3

import itertools
import statistics
import random
from collections import Counter
from typing import NamedTuple

from more_itertools import (
    chunked,
    first,
)
from tqdm import tqdm

from cards_generator import (
    generate_all_test_cards,
    Symbol,
)


class StrengthItem(NamedTuple):
    strength_value: int
    num_of_repeats: int


class SymbolsSuite:

    def __init__(self, initial_count: int):
        self._symbol_to_count = {symbol: initial_count for symbol in Symbol}

    def copy(self):
        new_instance = self.__class__(initial_count=0)
        new_instance._symbol_to_count.update(self)
        assert new_instance._symbol_to_count.keys() == self._symbol_to_count.keys()
        return new_instance

    def find_symbol_with_highest_count(self):
        highest_count_symbol = max(self._symbol_to_count.items(),
                                   key=lambda symbol_count: symbol_count[1])
        return highest_count_symbol

    def __iter__(self):
        return iter(self._symbol_to_count.items())

    def __getitem__(self, symbol):
        return self._symbol_to_count[symbol]

    def __setitem__(self, symbol, count):
        return self._symbol_to_count.__setitem__(symbol, count)

    def __str__(self):
        symbols = [f'{number:2} - {str(symbol).capitalize():8} -> {count}'
                   for number, (symbol, count)
                   in enumerate(self._symbol_to_count.items(), start=1)]
        chunked_symbols = ["\n".join(i) for i in chunked(symbols, 4)]
        return '\n\n'.join(chunked_symbols)

    def __format__(self, format_spec):
        if format_spec == 'short':
            if len(set(self._symbol_to_count.values())) == 1:
                symbols_count = first(self._symbol_to_count.values())
                return f'Wszystkie symbole -> {symbols_count}'
            else:
                return format(self)
        else:
            return super().__format__(format_spec)


class Test:

    @classmethod
    def initialize_all_cards(cls):
        cls._all_cards = tuple(generate_all_test_cards())

    def __init__(self,
                 symbols_suite,
                 colors_in_team,
                 how_many_runs_in_test,
                 how_many_cards_to_draw,
                 how_many_cards_to_test,
                 how_many_attack_cards_to_test):

        self.own_symbols_suite = symbols_suite.copy()
        self.how_many_runs_in_test = how_many_runs_in_test
        self.how_many_cards_to_draw = how_many_cards_to_draw
        self.how_many_cards_to_test = how_many_cards_to_test
        self.how_many_attack_cards_to_test = how_many_attack_cards_to_test
        self.def_cards_count = self.how_many_cards_to_test - how_many_attack_cards_to_test
        self.mean_attack_strength = 0
        self.mean_def_strength = 0
        self.median_attack_strength = 0
        self.median_def_strength = 0
        self.number_of_combinations = 0
        self.number_of_attack_def_combinations = 0
        self._already_used = False
        self._colors_in_team = colors_in_team
        self._data_to_calculate_mean_attack_strength = []
        self._data_to_calculate_mean_def_strength = []
        self._data_to_calculate_median_attack_strength = []
        self._data_to_calculate_median_def_strength = []

    def perform_run(self, run_index):
        run = TestRun(self.own_symbols_suite,
                      self._all_cards,
                      self._colors_in_team,
                      self.how_many_cards_to_draw,
                      self.how_many_cards_to_test,
                      self.how_many_attack_cards_to_test)

        run.perform()
        self._get_data_to_calculations(run)

        if run_index == 0:
            self.number_of_combinations = len(run.all_combinations_from_drawn_cards)
            self.number_of_attack_def_combinations = len(
                run.all_attack_combinations_from_drawn_cards)

        else:
            assert (self.number_of_combinations
                    == len(run.all_combinations_from_drawn_cards))
            assert (self.number_of_attack_def_combinations
                    == len(run.all_attack_combinations_from_drawn_cards)
                    == len(run.all_def_combinations_from_drawn_cards))

    def perform(self):
        self._ensure_using_only_once()
        for run_index, _ in enumerate(tqdm(range(self.how_many_runs_in_test))):
            self.perform_run(run_index)
        self._calculate_mean_strengths()
        self._calculate_median_strengths()

    def _ensure_using_only_once(self):
        if self._already_used:
            raise RuntimeError(f'{self!a} already used')
        self._already_used = True

    def _get_data_to_calculations(self, run_instance):
        self._data_to_calculate_mean_attack_strength.extend(
                                        run_instance.most_common_strength_in_attack_combinations)
        self._data_to_calculate_mean_def_strength.extend(
                                        run_instance.most_common_strength_in_def_combinations)
        self._data_to_calculate_median_attack_strength.extend(run_instance.data_to_median_attack)
        self._data_to_calculate_median_def_strength.extend(run_instance.data_to_median_def)

    def _calculate_mean_strengths(self):
        self.mean_attack_strength = statistics.mean(self._data_to_calculate_mean_attack_strength)
        self.mean_def_strength = statistics.mean(self._data_to_calculate_mean_def_strength)

    def _calculate_median_strengths(self):
        self.median_attack_strength = statistics.median(
                                                    self._data_to_calculate_median_attack_strength)
        self.median_def_strength = statistics.median(
                                                    self._data_to_calculate_median_def_strength)


class TestRun:

    def __init__(self,
                 symbols_to_use,
                 all_cards,
                 colors_in_team,
                 how_many_cards_to_draw,
                 how_many_cards_to_test,
                 how_many_attack_cards_to_test):

        self.all_combinations_from_drawn_cards = []
        self.all_attack_combinations_from_drawn_cards = []
        self.all_def_combinations_from_drawn_cards = []
        self.most_common_strength_in_attack_combinations = []
        self.most_common_strength_in_def_combinations = []
        self.data_to_median_attack = []
        self.data_to_median_def = []
        self._symbols_to_use = symbols_to_use
        self._colors_in_team = colors_in_team
        self._how_many_cards_to_draw = how_many_cards_to_draw
        self._how_many_cards_to_test = how_many_cards_to_test
        self._how_many_attack_cards_to_test = how_many_attack_cards_to_test
        self._already_used = False
        self._drawn_cards = []
        self._all_cards = all_cards
        self._total_cards_strength = 0

    def perform(self):
        self._ensure_using_only_once()
        self._draw_cards()
        self._set_all_possible_combinations()
        self._set_all_possible_attack_and_def_combinations()

        calculate_attack_combinations = TestRunCalculations(
                    symbols_to_use=self._symbols_to_use,
                    combination_type=self.all_attack_combinations_from_drawn_cards,
                    combination_type_mean_data=self.most_common_strength_in_attack_combinations,
                    combination_type_median_data=self.data_to_median_attack,
                    colors_in_team=self._colors_in_team)
        calculate_attack_combinations.perform_calculation_for_combination_type()

        calculate_def_combinations = TestRunCalculations(
                    symbols_to_use=self._symbols_to_use,
                    combination_type=self.all_def_combinations_from_drawn_cards,
                    combination_type_mean_data=self.most_common_strength_in_def_combinations,
                    combination_type_median_data=self.data_to_median_def,
                    colors_in_team=self._colors_in_team)
        calculate_def_combinations.perform_calculation_for_combination_type()

    def _ensure_using_only_once(self):
        if self._already_used:
            raise RuntimeError(f'{self!a} already used')
        self._already_used = True

    def _draw_cards(self):
        assert self._drawn_cards == []
        cards = random.sample(self._all_cards, self._how_many_cards_to_draw)
        self._drawn_cards.extend(cards)

    def _set_all_possible_combinations(self):
        assert self.all_combinations_from_drawn_cards == []

        self.all_combinations_from_drawn_cards.extend([
            combination
            for combination in itertools.combinations(self._drawn_cards,
                                                      self._how_many_cards_to_test)])

    def _set_all_possible_attack_and_def_combinations(self):
        assert self.all_attack_combinations_from_drawn_cards == []
        assert self.all_def_combinations_from_drawn_cards == []

        for combination in self.all_combinations_from_drawn_cards:
            for attack_combination in itertools.combinations(combination,
                                                             self._how_many_attack_cards_to_test):
                self.all_attack_combinations_from_drawn_cards.append(attack_combination)
                self.all_def_combinations_from_drawn_cards.append(
                    list(card for card
                         in combination
                         if card not in attack_combination))


class TestRunCalculations:

    def __init__(self,
                 symbols_to_use,
                 combination_type,
                 combination_type_mean_data,
                 combination_type_median_data,
                 colors_in_team):

        self.combination_type = combination_type
        self.combination_type_mean_data = combination_type_mean_data
        self.top_20_percent_of_scores = combination_type_median_data
        self._colors_in_team = colors_in_team
        self._colors_from_cards_indicator = 0
        self._strength_from_colors = 0
        self._strength_value_of_combinations = []
        self._most_common_combinations_to_calculate_mean_strength_value = []
        self._most_common_strength_items = []
        self._strength_values_when_more_than_one_most_common = []
        self._number_of_repeats_of_highest_strength_items = 0
        self._symbols_in_team = symbols_to_use
        self._data_to_calc_median_one_combination = []

    def perform_calculation_for_combination_type(self):
        self._calculate_value_of_combinations(combination_type=self.combination_type)
        self._find_most_common_strength_of_combinations()
        self._get_number_of_repeats_to_calculate_mean_value()
        self._provide_strength_of_most_common_combinations()
        self._provide_strength_value_to_calc_mean(
                                        combination_type_mean_data=self.combination_type_mean_data)
        self._provide_strength_value_to_calc_mean_when_more_most_common_values(
                                        combination_type_mean_data=self.combination_type_mean_data)
        self._find_top_results_of_combination(self.top_20_percent_of_scores)

    def _calculate_symbols_in_combination(self, combination):
        for card in combination:
            for symbol_on_card in card.symbols:
                self._symbols_on_cards[symbol_on_card] += 1

    def _calculate_cards_strength_in_combination(self, combination):
        self._total_cards_strength = sum(card.strength for card in combination)

    def _calculate_colors_in_combination(self, combination):
        self._colors_from_cards_indicator = 0

        for card in combination:
            if card.color in self._colors_in_team:
                self._colors_from_cards_indicator += 1

    def _set_useless_symbols_count_to_zero(self):
        for symbol, count in self._symbols_on_cards:
            if self._symbols_in_team[symbol] == 0:
                self._symbols_on_cards[symbol] = 0

    def _use_colors(self):
        while self._colors_from_cards_indicator:
            symbol, count = self._symbols_on_cards.find_symbol_with_highest_count()
            if self._total_cards_strength <= count:
                self._total_cards_strength += 1
            else:
                self._symbols_on_cards[symbol] += 1
            self._colors_from_cards_indicator -= 1

    def _get_multiplied_symbols(self):
        multiplied_symbols = [(symbol_on_card_value * symbol_in_team_value)
                              for (symbol_on_card, symbol_on_card_value),
                                  (symbol_in_team, symbol_in_team_value)
                              in zip(self._symbols_on_cards,
                                     self._symbols_in_team)]
        return multiplied_symbols

    @staticmethod
    def _get_combination_strength(multiplied_symbols,
                                  total_cards_strength):

        combination_strength = sum(multiplied_symbols) * total_cards_strength

        return combination_strength

    def _calculate_value_of_combinations(self, combination_type):
        for combination in combination_type:
            self._scores_of_combination = []
            self._symbols_on_cards = SymbolsSuite(0)
            self._calculate_symbols_in_combination(combination)
            self._calculate_cards_strength_in_combination(combination)
            self._calculate_colors_in_combination(combination)
            self._set_useless_symbols_count_to_zero()
            self._use_colors()

            total_combination_strength = self._get_combination_strength(
                                                                self._get_multiplied_symbols(),
                                                                self._total_cards_strength)

            self._strength_value_of_combinations.append(total_combination_strength)
            self._data_to_calc_median_one_combination.append(total_combination_strength)

    def _find_most_common_strength_of_combinations(self):
        self._most_common_strength_items.extend(
            [StrengthItem(strength_value, num_of_repeats)
             for strength_value, num_of_repeats
             in Counter(self._strength_value_of_combinations).most_common()])

    def _find_top_results_of_combination(self, combination_type_median_data):
        top_20_count = int(round(0.2 * len(self._data_to_calc_median_one_combination)))
        combination_type_median_data.extend(
            sorted(self._data_to_calc_median_one_combination[:top_20_count], reverse=True))

    def _get_number_of_repeats_to_calculate_mean_value(self):
        self._number_of_repeats_of_highest_strength_items = self._most_common_strength_items[0][1]

    def _provide_strength_of_most_common_combinations(self):
        for item in self._most_common_strength_items:
            if item.num_of_repeats == self._number_of_repeats_of_highest_strength_items:
                if item not in self._most_common_combinations_to_calculate_mean_strength_value:
                    self._most_common_combinations_to_calculate_mean_strength_value.append(
                                                                            item.strength_value)

    def _provide_strength_value_to_calc_mean(self, combination_type_mean_data):
        for item in self._most_common_strength_items:
            if item.num_of_repeats == self._number_of_repeats_of_highest_strength_items:
                if len(self._most_common_combinations_to_calculate_mean_strength_value) < 2:
                    combination_type_mean_data.append(item.strength_value)

    def _provide_strength_value_to_calc_mean_when_more_most_common_values(
                                                                    self,
                                                                    combination_type_mean_data):

        if len(self._most_common_combinations_to_calculate_mean_strength_value) > 1:
            for item in self._most_common_combinations_to_calculate_mean_strength_value:
                self._strength_values_when_more_than_one_most_common.append(item)

            temporary_mean_value = statistics.mean(
                                            self._strength_values_when_more_than_one_most_common)

            combination_type_mean_data.append(temporary_mean_value)
