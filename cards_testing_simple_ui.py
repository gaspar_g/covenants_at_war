from datetime import datetime

from cards_generator import (
    Color,
    Symbol,
)
from cards_testing import (
    SymbolsSuite,
    Test,
)

RESULT_FILE_NAME = 'Card_tests.txt'

MENU_TEXT = ('---------------------------'
             '\n1 - Rozpocznij testowanie'
             '\n2 - Zmień liczbę symboli w drużynie '
             '(domyślna liczba dla każdego symbolu: 0)'
             '\nquit - Zakończ\n')


def get_non_negative_integer_from_user(text):
    while True:
        print(text)
        try:
            value = int(input())
            if value < 0:
                raise ValueError
            return value

        except ValueError:
            print('Nieprawidłowa wartość. Wprowadź liczbę całkowitą ≥ 0.\n')


def set_colors_in_team(colors_in_team):
    set_team_text = ('\nWskaż klasy w drużynie:'
                     '\n1 -> Warrior'
                     '\n2 -> Rogue'
                     '\n3 -> Priest'
                     '\n4 -> Mage'
                     '\nWpisz cyfry reprezentujące klasy w jednej linii, np.: 124'
                     '\nLiczba postaci w drużynie: 1-4')

    accept_team_text = ('Akceptujesz podany skład drużyny?'
                        '\nWpisz: t albo n | t = tak, n = nie\n')

    while True:
        print(set_team_text)
        try:
            user_input = input()
            if (len(user_input)) < 0 and (len(user_input) <= 4):
                if '1' in user_input:
                    colors_in_team[Color.brown] = True
                if '2' in user_input:
                    colors_in_team[Color.gray] = True
                if '3' in user_input:
                    colors_in_team[Color.purple] = True
                if '4' in user_input:
                    colors_in_team[Color.orange] = True

            for i in user_input:
                if i not in '1234':
                    raise ValueError
            else:
                user_accept_input = input(accept_team_text)
                if user_accept_input.lower() == 't':
                    return colors_in_team

        except ValueError:
            print('Nieprawidłowa wartość. Spróbuj ponownie. '
                  '\nPrzy wybieraniu drużyny używaj jedynie kombinacji '
                  'cyfr 1, 2, 3, 4.'
                  '\nLiczba postaci w drużynie: 1-4.')


def change_symbol_values(symbols_suite):
    while True:
        print(symbols_suite)
        try:
            which_symbol = get_non_negative_integer_from_user(
                '\nWskaż typ symbolu (liczbę porządkową), którego liczbę chcesz zmienić.\n'
                'Jeśli chcesz zakończyć zmienianie, wpisz: 0\n')
            if 1 <= which_symbol <= 16:
                sym = Symbol(which_symbol)
                symbols_suite[sym] = get_non_negative_integer_from_user('Wprowadź liczbę:\n')

            elif which_symbol == 0:
                break
            else:
                raise ValueError

        except ValueError:
            print('\nSpróbuj ponownie podając właściwą liczbę.\n')


def provide_basic_test_summary(test_instance):
    basic_test_summary = (
            f'\n\nTestowałeś kombinacje {test_instance.how_many_cards_to_test} '
            f'z {test_instance.how_many_cards_to_draw} kart'
            f'\nŁączna liczba kombinacji: {test_instance.number_of_combinations}'
            f'\nLiczba kombinacji (atak/obrona): {test_instance.number_of_attack_def_combinations}'
            f'\nLiczba przebiegów: {test_instance.how_many_runs_in_test}'
            f'\nLiczba kart ataku: {test_instance.how_many_attack_cards_to_test}'
            f'\nLiczba kart obrony: {test_instance.def_cards_count}'
            f'\nŚrednia arytmetyczna (atak): {test_instance.mean_attack_strength:.2f}.'
            f'\nŚrednia arytmetyczna (obrona): {test_instance.mean_def_strength:.2f}.'
            f'\nMediana (atak): {test_instance.median_attack_strength:.2f}.'
            f'\nMediana (obrona): {test_instance.median_def_strength:.2f}.')

    return basic_test_summary


def provide_extended_test_summary(test_instance):
    basic_test_summary = provide_basic_test_summary(test_instance)

    extended_summary = (
        f'--- Początek testu ({datetime.now():%Y-%m-%d %H:%M:%S}) ---'
        f'\n\n{test_instance.own_symbols_suite:short}'
        f'{basic_test_summary}'
        f'\n\n--- Koniec testu ---\n\n')

    return extended_summary


def save_test_result_to_file(test_instance,
                             file_name):
    summary = provide_extended_test_summary(test_instance)
    with open(file_name, 'a') as file:
        file.write(f'{summary}')


def get_test_parameters_from_user():
    colors_in_team = {Color.brown: False,
                      Color.gray: False,
                      Color.purple: False,
                      Color.orange: False}

    set_colors_in_team(colors_in_team)
    how_many_runs = get_non_negative_integer_from_user('Ile przebiegów chcesz wykonać?\n')

    if how_many_runs == 0:
        return

    how_many_cards_to_draw = get_non_negative_integer_from_user('Ile kart chcesz dociągnąć?\n')
    how_many_cards_to_test = get_non_negative_integer_from_user(
        f'Ile kart chcesz testować? Maksymalna liczba to: {how_many_cards_to_draw}\n')

    if how_many_cards_to_test > how_many_cards_to_draw:
        print(f'Nie możesz testować więcej kart niż wylosowałeś.\n'
              f'Testujemy maksymalną liczbę kart - {how_many_cards_to_draw}')
        how_many_cards_to_test = how_many_cards_to_draw

    how_many_attack_cards_to_test = get_non_negative_integer_from_user(
        f'Ile kart chcesz przeznaczyć na atak? Maksymalna liczba to: {how_many_cards_to_test}\n')

    if how_many_attack_cards_to_test > how_many_cards_to_test:
        print(f'Nie możesz mieć w ataku więcej kart niż liczba kart do testu. '
              f'Testujemy maksymalną liczbę kart w ataku - {how_many_cards_to_test}')
        how_many_attack_cards_to_test = how_many_cards_to_test

    return (colors_in_team,
            how_many_runs,
            how_many_cards_to_draw,
            how_many_cards_to_test,
            how_many_attack_cards_to_test)


def perform_test(symbols_to_use):

    (colors_in_team, how_many_runs, how_many_cards_to_draw, how_many_cards_to_test,
     how_many_attack_cards_to_test) = get_test_parameters_from_user()

    single_test = Test(symbols_to_use,
                       colors_in_team,
                       how_many_runs,
                       how_many_cards_to_draw,
                       how_many_cards_to_test,
                       how_many_attack_cards_to_test)

    single_test.perform()
    save_test_result_to_file(single_test, RESULT_FILE_NAME)

    test_summary = provide_basic_test_summary(single_test)

    print(f'\n\nTest zakończony.'
          f'{test_summary}'
          f'\n\nWyniki testu zostały zapisane w pliku {RESULT_FILE_NAME}')


def perform_card_testing():
    symbols_to_use = SymbolsSuite(initial_count=0)
    Test.initialize_all_cards()

    while True:
        decide = input(MENU_TEXT)
        if decide == '1':
            perform_test(symbols_to_use)
        elif decide == '2':
            change_symbol_values(symbols_to_use)
        elif decide == 'quit':
            return
        else:
            print('Błąd. Wybierz ponownie.')
